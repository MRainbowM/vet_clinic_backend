-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_vet_clinic
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `workers_worker`
--

DROP TABLE IF EXISTS `workers_worker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workers_worker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `patronymic` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `visibility` tinyint(1) NOT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  `info` longtext NOT NULL,
  `position_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `workers_worker_position_id_6f29c0fb_fk_workers_position_id` (`position_id`),
  CONSTRAINT `workers_worker_position_id_6f29c0fb_fk_workers_position_id` FOREIGN KEY (`position_id`) REFERENCES `workers_position` (`id`),
  CONSTRAINT `workers_worker_user_id_387e1800_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workers_worker`
--

LOCK TABLES `workers_worker` WRITE;
/*!40000 ALTER TABLE `workers_worker` DISABLE KEYS */;
INSERT INTO `workers_worker` VALUES (1,'Иванова','Ирина','Ивановна','2345678765432','sdfg@mail.ru','upload/doctor.2bc29fc7_rGajMAQ.jpeg','2019-12-12',1,NULL,'Стаж работы более 15 лет!',1,10),(2,'Алексеев','Александр','Александрович','12345678987654','sdfg@mail.ru','upload/dentistry.43122a1b.jpg','2019-12-15',1,NULL,'',2,NULL),(3,'Иванов','Иван','Иванович','1234567','sdfg@mail.ru','upload/cardiology.5a338940.jpg','2019-12-15',1,NULL,'',3,NULL),(4,'Доктров','Айболит','Александрович','12345','sdfg@mail.ru','upload/surgery.2d9abafc.jpg','2019-12-15',1,NULL,'',1,NULL),(5,'Петров','Петр','Айболитович','12345','sdfg@mail.ru','upload/chiping.860926c4_WlgKBkb.jpg','2019-12-15',1,NULL,'',1,NULL),(6,'Врачевский','Андрей','Петрович','234567890','sdfg@mail.ru','','2019-12-17',1,NULL,'Лучший хирург города',2,NULL);
/*!40000 ALTER TABLE `workers_worker` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-24  1:25:26
