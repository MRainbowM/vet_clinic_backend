-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_vet_clinic
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `visits_visit_services`
--

DROP TABLE IF EXISTS `visits_visit_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visits_visit_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `visits_visit_services_visit_id_service_id_55554597_uniq` (`visit_id`,`service_id`),
  KEY `visits_visit_services_service_id_9f99abf3_fk_services_service_id` (`service_id`),
  CONSTRAINT `visits_visit_services_service_id_9f99abf3_fk_services_service_id` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`),
  CONSTRAINT `visits_visit_services_visit_id_f68cd442_fk_visits_visit_id` FOREIGN KEY (`visit_id`) REFERENCES `visits_visit` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits_visit_services`
--

LOCK TABLES `visits_visit_services` WRITE;
/*!40000 ALTER TABLE `visits_visit_services` DISABLE KEYS */;
INSERT INTO `visits_visit_services` VALUES (3,3,1),(4,5,1),(6,5,3),(7,7,1),(8,7,3),(9,7,4),(10,8,1),(11,8,2),(12,8,4),(43,40,3),(44,41,4),(45,41,5),(46,42,2),(47,42,3),(48,42,5),(49,43,2),(50,43,4),(51,43,6),(52,44,2),(53,44,4),(54,44,6),(55,45,3),(56,45,4),(57,45,5);
/*!40000 ALTER TABLE `visits_visit_services` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-24  1:25:34
