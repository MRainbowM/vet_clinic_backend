-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_vet_clinic
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-12-12 10:31:16.198900'),(2,'auth','0001_initial','2019-12-12 10:31:16.362581'),(3,'admin','0001_initial','2019-12-12 10:31:16.863459'),(4,'admin','0002_logentry_remove_auto_add','2019-12-12 10:31:16.998836'),(5,'admin','0003_logentry_add_action_flag_choices','2019-12-12 10:31:17.010245'),(6,'contenttypes','0002_remove_content_type_name','2019-12-12 10:31:17.159118'),(7,'auth','0002_alter_permission_name_max_length','2019-12-12 10:31:17.237979'),(8,'auth','0003_alter_user_email_max_length','2019-12-12 10:31:17.266747'),(9,'auth','0004_alter_user_username_opts','2019-12-12 10:31:17.281130'),(10,'auth','0005_alter_user_last_login_null','2019-12-12 10:31:17.361481'),(11,'auth','0006_require_contenttypes_0002','2019-12-12 10:31:17.365946'),(12,'auth','0007_alter_validators_add_error_messages','2019-12-12 10:31:17.376857'),(13,'auth','0008_alter_user_username_max_length','2019-12-12 10:31:17.451260'),(14,'auth','0009_alter_user_last_name_max_length','2019-12-12 10:31:17.520699'),(15,'auth','0010_alter_group_name_max_length','2019-12-12 10:31:17.542031'),(16,'auth','0011_update_proxy_permissions','2019-12-12 10:31:17.551451'),(17,'clients','0001_initial','2019-12-12 10:31:17.688365'),(18,'services','0001_initial','2019-12-12 10:31:17.912093'),(19,'filials','0001_initial','2019-12-12 10:31:18.126019'),(20,'workers','0001_initial','2019-12-12 10:31:18.366008'),(21,'reviews','0001_initial','2019-12-12 10:31:18.600931'),(22,'sales','0001_initial','2019-12-12 10:31:18.893578'),(23,'sessions','0001_initial','2019-12-12 10:31:19.065534'),(24,'services','0002_direction_photo','2019-12-15 12:15:28.314784'),(25,'services','0003_direction_date_of_delete','2019-12-15 12:24:39.431694'),(26,'sales','0002_sale_photo','2019-12-15 12:50:33.419185'),(27,'filials','0002_auto_20191217_1436','2019-12-17 06:37:08.750824'),(28,'visits','0001_initial','2019-12-17 06:37:08.843611'),(29,'clients','0002_auto_20191217_1459','2019-12-17 06:59:59.972339'),(30,'clients','0003_auto_20191217_1636','2019-12-17 08:36:49.866818'),(31,'filials','0003_auto_20191217_1801','2019-12-17 10:01:49.612661'),(32,'visits','0002_auto_20191217_1801','2019-12-17 10:01:50.614468'),(33,'reviews','0002_auto_20191220_0945','2019-12-20 01:45:45.328767'),(34,'visits','0003_auto_20191220_0945','2019-12-20 01:45:45.554738'),(35,'reviews','0003_review_name','2019-12-20 02:18:38.105679'),(36,'reviews','0004_review_date','2019-12-20 02:22:54.885484'),(37,'workers','0002_worker_user','2019-12-21 06:37:07.669386'),(38,'visits','0004_visit_date_of_delete','2019-12-21 07:35:20.491037'),(39,'visits','0005_visit_duration','2019-12-21 08:39:31.400404'),(40,'visits','0006_visit_name','2019-12-22 02:35:48.380811'),(41,'visits','0007_auto_20191222_1157','2019-12-22 03:57:50.006687'),(42,'visits','0008_auto_20191222_1201','2019-12-22 04:06:45.198954'),(43,'visits','0009_auto_20191222_1206','2019-12-22 04:06:45.317631'),(44,'visits','0010_auto_20191222_1217','2019-12-22 04:17:57.367133'),(45,'clients','0004_auto_20191222_1413','2019-12-22 06:13:31.541516'),(46,'visits','0011_auto_20191222_1957','2019-12-22 11:57:14.614735'),(47,'reviews','0005_feedback','2019-12-22 18:36:47.855332');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-24  1:25:27
