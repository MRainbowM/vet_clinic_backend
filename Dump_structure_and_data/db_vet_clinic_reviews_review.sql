-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_vet_clinic
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `reviews_review`
--

DROP TABLE IF EXISTS `reviews_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `star` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `comment` longtext NOT NULL,
  `visibility` tinyint(1) NOT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reviews_review_doctor_id_86f0f007_fk_workers_worker_id` (`doctor_id`),
  KEY `reviews_review_service_id_8aaf292d_fk_services_service_id` (`service_id`),
  KEY `reviews_review_client_id_80319127_fk_clients_client_id` (`client_id`),
  CONSTRAINT `reviews_review_client_id_80319127_fk_clients_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients_client` (`id`),
  CONSTRAINT `reviews_review_doctor_id_86f0f007_fk_workers_worker_id` FOREIGN KEY (`doctor_id`) REFERENCES `workers_worker` (`id`),
  CONSTRAINT `reviews_review_service_id_8aaf292d_fk_services_service_id` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews_review`
--

LOCK TABLES `reviews_review` WRITE;
/*!40000 ALTER TABLE `reviews_review` DISABLE KEYS */;
INSERT INTO `reviews_review` VALUES (1,5,'Все окококококо','',1,NULL,1,NULL,1,NULL,NULL,NULL,NULL),(2,2,'СУПЕЕЕЕЕЕЕЕРРРР','ададда',1,NULL,1,1,NULL,NULL,NULL,NULL,NULL),(3,3,'ВСЕ ПЛОХА!!!!!!!!!!!!!!!!!!!!1','НЕ ГОНИ',1,NULL,1,1,NULL,NULL,NULL,NULL,NULL),(7,5,'Все супер','',1,NULL,1,NULL,4,'sdfg@mail.ru','9898989898','Петр','2019-12-20 10:33:36.638425'),(8,1,'ВСЕ ОЧЕНЬ ПЛОХА! УВОЛЬТЕ ЭТОГО ЧЕЛОВЕКА','ты чо врешь а',1,NULL,NULL,2,NULL,'hghgfv@mail.ru',NULL,'Мария','2019-12-20 10:34:20.850117'),(10,5,'Очень классно меня подстриг, мяу','Лафки-скамейки :3',1,NULL,NULL,6,NULL,'pochta@gmail.com','2345678','Андрюша','2019-12-20 11:43:29.346063'),(11,5,'Доктор айболит какашка','',1,NULL,1,4,NULL,'sdfg@mail.ru','9898989898','Петр','2019-12-20 14:51:50.728504'),(12,1,'','',1,NULL,NULL,NULL,3,'asdad@sdfsdf.ru','sadasda345345','ясчясчяс','2019-12-20 15:50:12.416143'),(13,5,'Крутой чел','',1,NULL,1,2,NULL,'sdfg@mail.ru','9898989898','Петр','2019-12-23 02:56:25.860103'),(14,1,'dvfddfx','',1,NULL,1,2,NULL,'sdfg@mail.ru','9898989898','Петр','2019-12-23 02:57:59.267445'),(15,1,'szdvfszd','',1,NULL,1,2,NULL,'sdfg@mail.ru','9898989898','Петр','2019-12-23 02:58:20.827823'),(16,1,'','',1,NULL,1,2,NULL,'sdfg@mail.ru','9898989898','Петр','2019-12-23 03:00:49.736612');
/*!40000 ALTER TABLE `reviews_review` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-24  1:25:29
