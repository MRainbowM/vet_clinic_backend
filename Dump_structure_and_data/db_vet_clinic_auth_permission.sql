-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_vet_clinic
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add Клиент',1,'add_client'),(2,'Can change Клиент',1,'change_client'),(3,'Can delete Клиент',1,'delete_client'),(4,'Can view Клиент',1,'view_client'),(5,'Can add Вид животного',2,'add_kind'),(6,'Can change Вид животного',2,'change_kind'),(7,'Can delete Вид животного',2,'delete_kind'),(8,'Can view Вид животного',2,'view_kind'),(9,'Can add Питомец',3,'add_pet'),(10,'Can change Питомец',3,'change_pet'),(11,'Can delete Питомец',3,'delete_pet'),(12,'Can view Питомец',3,'view_pet'),(13,'Can add Должность',4,'add_position'),(14,'Can change Должность',4,'change_position'),(15,'Can delete Должность',4,'delete_position'),(16,'Can view Должность',4,'view_position'),(17,'Can add Работник',5,'add_worker'),(18,'Can change Работник',5,'change_worker'),(19,'Can delete Работник',5,'delete_worker'),(20,'Can view Работник',5,'view_worker'),(21,'Can add Отзыв',6,'add_review'),(22,'Can change Отзыв',6,'change_review'),(23,'Can delete Отзыв',6,'delete_review'),(24,'Can view Отзыв',6,'view_review'),(25,'Can add review_photo',7,'add_review_photo'),(26,'Can change review_photo',7,'change_review_photo'),(27,'Can delete review_photo',7,'delete_review_photo'),(28,'Can view review_photo',7,'view_review_photo'),(29,'Can add cabinet',8,'add_cabinet'),(30,'Can change cabinet',8,'change_cabinet'),(31,'Can delete cabinet',8,'delete_cabinet'),(32,'Can view cabinet',8,'view_cabinet'),(33,'Can add Филиал',9,'add_filial'),(34,'Can change Филиал',9,'change_filial'),(35,'Can delete Филиал',9,'delete_filial'),(36,'Can view Филиал',9,'view_filial'),(37,'Can add sale',10,'add_sale'),(38,'Can change sale',10,'change_sale'),(39,'Can delete sale',10,'delete_sale'),(40,'Can view sale',10,'view_sale'),(41,'Can add Услуга',11,'add_service'),(42,'Can change Услуга',11,'change_service'),(43,'Can delete Услуга',11,'delete_service'),(44,'Can view Услуга',11,'view_service'),(45,'Can add Направление',12,'add_direction'),(46,'Can change Направление',12,'change_direction'),(47,'Can delete Направление',12,'delete_direction'),(48,'Can view Направление',12,'view_direction'),(49,'Can add log entry',13,'add_logentry'),(50,'Can change log entry',13,'change_logentry'),(51,'Can delete log entry',13,'delete_logentry'),(52,'Can view log entry',13,'view_logentry'),(53,'Can add permission',14,'add_permission'),(54,'Can change permission',14,'change_permission'),(55,'Can delete permission',14,'delete_permission'),(56,'Can view permission',14,'view_permission'),(57,'Can add group',15,'add_group'),(58,'Can change group',15,'change_group'),(59,'Can delete group',15,'delete_group'),(60,'Can view group',15,'view_group'),(61,'Can add user',16,'add_user'),(62,'Can change user',16,'change_user'),(63,'Can delete user',16,'delete_user'),(64,'Can view user',16,'view_user'),(65,'Can add content type',17,'add_contenttype'),(66,'Can change content type',17,'change_contenttype'),(67,'Can delete content type',17,'delete_contenttype'),(68,'Can view content type',17,'view_contenttype'),(69,'Can add session',18,'add_session'),(70,'Can change session',18,'change_session'),(71,'Can delete session',18,'delete_session'),(72,'Can view session',18,'view_session'),(73,'Can add visit',19,'add_visit'),(74,'Can change visit',19,'change_visit'),(75,'Can delete visit',19,'delete_visit'),(76,'Can view visit',19,'view_visit'),(77,'Can add feedback',20,'add_feedback'),(78,'Can change feedback',20,'change_feedback'),(79,'Can delete feedback',20,'delete_feedback'),(80,'Can view feedback',20,'view_feedback');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-24  1:25:23
