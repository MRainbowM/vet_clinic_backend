-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_vet_clinic
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `workers_worker_services`
--

DROP TABLE IF EXISTS `workers_worker_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workers_worker_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `workers_worker_services_worker_id_service_id_458ad41e_uniq` (`worker_id`,`service_id`),
  KEY `workers_worker_servi_service_id_29191232_fk_services_` (`service_id`),
  CONSTRAINT `workers_worker_servi_service_id_29191232_fk_services_` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`),
  CONSTRAINT `workers_worker_services_worker_id_7868b4d3_fk_workers_worker_id` FOREIGN KEY (`worker_id`) REFERENCES `workers_worker` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workers_worker_services`
--

LOCK TABLES `workers_worker_services` WRITE;
/*!40000 ALTER TABLE `workers_worker_services` DISABLE KEYS */;
INSERT INTO `workers_worker_services` VALUES (1,1,1),(2,1,2),(5,1,5),(3,2,1),(4,2,5),(6,3,1),(7,3,2),(8,3,3),(9,3,4),(10,3,5),(11,3,6),(12,4,1),(13,4,2),(14,4,3),(15,4,4),(16,4,5),(17,4,6),(18,5,1),(19,5,2),(20,5,3),(21,5,4),(22,5,5),(23,5,6),(24,6,2),(25,6,3),(26,6,4);
/*!40000 ALTER TABLE `workers_worker_services` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-24  1:25:25
