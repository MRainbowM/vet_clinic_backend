let currentDate = new Date()
let viewDays = 7

let root = document.querySelector('.planer') //корн эл
let grid = root && root.querySelector('.planer-grid')

function cleanGrid() {
    grid.innerHTML = ''
}

function checkViewDays() {
    if (window.innerWidth <= 600) {
        viewDays = 1
    }
    else {
        viewDays = 7
    }
}

function createCol(modeClass = []) {
    let elem = document.createElement('div')
    elem.classList.add('planer-grid__col', ...modeClass)
    grid.append(elem)
    return elem
}

function renderTimeCol(fromTime, toTime, timeInterval) {
    let elem = createCol(['time'])
    let htmlStr = '<div class="cell"></div>'
    for (let i = fromTime; i < toTime; i = i + timeInterval) {
        let timeStrArr = new Date(i).toTimeString().split(':')
        let timeStr = timeStrArr[0] + ':' + timeStrArr[1]
        htmlStr += `<div class="cell"><span>${timeStr}</span></div>`
    }    
    elem.innerHTML = htmlStr
}

function renderDayCol(fromTime, toTime, timeInterval, tasks) {
    let date = new Date(fromTime)
    let elem = createCol()
    let weekNames = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'] 
    let dateStrArr = date.toISOString().split('T')[0].split('-')
    let nameWeek = weekNames[date.getDay()]
    let dateStr = `${dateStrArr[2]}.${dateStrArr[1]} (${nameWeek})`
    let htmlStr = `<div class="cell">${dateStr}</div>`
    for (let i = fromTime; i < toTime; i = i + timeInterval) {
        let task = tasks.find(el => el.from === i)
        if (task) {
            htmlStr += '<div class="planer-grid__task">'
            let toTaskTime = task.from + (task.duration * 60 * 1000)
            for (let j = i; j < toTaskTime; j = j + timeInterval) {
                htmlStr += '<div class="cell"><span></span></div>'
                i = j
            }
            htmlStr += `<a href="/lk/visit/${task.id}" class="planer-task cropping"><span>${task.name}</span></a href="{% url 'lk:client_visit_card' visit.id %}">`
            htmlStr += '</div>'
        } else {
            htmlStr += '<div class="cell"><span></span></div>'
        }
    }
    elem.innerHTML = htmlStr
}

function getStartWeek(date) {
    let difDay = date.getDay() - 1 //разность дней
    let startWeek = date.getTime() - (difDay * 24 * 60 * 60 * 1000)
    return new Date(startWeek)
}

function renderGrid(from, to, tasks) {
    cleanGrid()
    let timeInterval = 20 * 60 * 1000
    let fromTime = from.getTime()
    let toTime = to.getTime()
    renderTimeCol(fromTime, toTime, timeInterval)
    for (let i = 0; i < viewDays; i++) {
        renderDayCol(fromTime, toTime, timeInterval, tasks)
        fromTime += 24 * 60 * 60 * 1000
        toTime += 24 * 60 * 60 * 1000
    }
}

function renderSelect(from) {
    let elem = root.querySelector('.planer-select span')
    let toTime = from.getTime() + (viewDays - 1) * 24 * 60 * 60 * 1000
    let to = new Date(toTime)
    let fromStrArr = from.toISOString().split('T')[0].split('-')
    let fromStr = `${fromStrArr[2]}.${fromStrArr[1]}`
    let toStrArr = to.toISOString().split('T')[0].split('-')
    let toStr = `${toStrArr[2]}.${toStrArr[1]}`
    elem.textContent = `${fromStr} - ${toStr}`
}

function main() {
    let from = new Date(+window.PlanerFrom)
    let to = new Date(+window.PlanerTo)
    let tasks = window.PlanerData
    // let tasks = [
    //     {
    //         id: 1,
    //         name: 'Стерилизация',
    //         from: new Date(2019, 9, 20, 9, 0).getTime(),
    //         duration: 20
    //     },
    //     {
    //         id: 2,
    //         name: 'Стерилизация',
    //         from: new Date(2019, 9, 20, 10, 20).getTime(),
    //         duration: 20
    //     },
    //     {
    //         id: 3,
    //         name: 'Стерилизация',
    //         from: new Date(2019, 9, 19, 8, 0).getTime(),
    //         duration: 40
    //     },
    //     {
    //         id: 4,
    //         name: 'Стерилизация',
    //         from: new Date(2019, 9, 17, 8, 40).getTime(),
    //         duration: 60
    //     },
    //     {
    //         id: 5,
    //         name: 'Стерилизация',
    //         from: new Date(2019, 9, 14, 9, 40).getTime(),
    //         duration: 60
    //     }
    // ]
    checkViewDays()
    renderSelect(from)
    renderGrid(from, to, tasks)
    window.addEventListener('resize', () => {
        checkViewDays()
        renderSelect(from)
        renderGrid(from, to, tasks)
    })
}

if (root) {
    main()
}
