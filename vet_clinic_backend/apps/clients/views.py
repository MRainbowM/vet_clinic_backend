from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from .models import *
from visits.models import Visit
from django.contrib.auth.models import User
from django.core.paginator import Paginator

def adm_list(request):
    if request.user.is_authenticated:
        if not request.user.groups.filter(name='managers').exists():
            raise PermissionDenied

    else:
        return HttpResponseRedirect('/accounts/login')
    clients = Client.objects.all().order_by('id')
    pets_all = Pet.objects.filter(date_of_delete = None)
    visits_all = Visit.objects.all()
    paginator = Paginator(clients, 4)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        clients_all = paginator.page(page)
    except(EmptyPage, InvalidPage):
        clients_all = paginator.page(paginator.num_pages)



    return render(request, 'clients/adm-clients.html', {'clients_all':clients_all, 'pets_all':pets_all, 'visits_all':visits_all})


    