# Generated by Django 2.2.7 on 2019-12-12 10:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('services', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cabinet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Название')),
                ('description', models.TextField(blank=True, verbose_name='Описание')),
                ('state_work', models.BooleanField(default=True, verbose_name='Статус работы')),
                ('services', models.ManyToManyField(blank=True, to='services.Service', verbose_name='Услуги в кабинете')),
            ],
        ),
        migrations.CreateModel(
            name='Filial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address_full', models.CharField(max_length=200, verbose_name='Полный адрес')),
                ('address', models.CharField(max_length=200, verbose_name='Короткий адрес')),
                ('mail', models.CharField(max_length=50, verbose_name='Почта')),
                ('date_of_delete', models.CharField(blank=True, max_length=20, null=True, verbose_name='Дата удаления')),
                ('visibility', models.BooleanField(default=True, verbose_name='Видимость на сайте')),
                ('cabinets', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='filials.Cabinet', verbose_name='Кабинеты')),
            ],
            options={
                'verbose_name': 'Филиал',
                'verbose_name_plural': 'Филиалы',
            },
        ),
    ]
