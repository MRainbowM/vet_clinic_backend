from django.shortcuts import render, redirect
from reviews.forms import FeedbackForm

def client_contacts(request):
    if request.method == "POST":
        form = FeedbackForm(request.POST)
        if form.is_valid():
            new_review = form.save()
            return redirect('filials:client_message')
        else:
            return render(request, 'filials/contacts.html', {'form':form})
    else:
        form = FeedbackForm()

    return render(request, 'filials/contacts.html', {'form':form})

def client_message(request):
    return render(request, 'filials/success_message.html', {})

