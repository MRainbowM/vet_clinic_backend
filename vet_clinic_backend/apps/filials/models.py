from django.db import models
from django.core.validators import MinValueValidator
from services.models import Service

class Cabinet(models.Model):
    name = models.CharField('Название', max_length = 50)
    description = models.TextField('Описание', blank=True)
    state_work = models.BooleanField('Статус работы', default=True)
    services = models.ManyToManyField(Service, blank=True, verbose_name='Услуги в кабинете')
    class Meta:
        verbose_name = 'Кабинет'
        verbose_name_plural = 'Кабинеты'    

class Filial(models.Model):
    address_full = models.CharField('Полный адрес', max_length = 200)
    address = models.CharField('Короткий адрес', max_length = 200)
    mail = models.CharField('Почта', max_length = 50)
    cabinets = models.ForeignKey(Cabinet, on_delete = models.CASCADE, verbose_name='Кабинеты')
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)
    visibility = models.BooleanField('Видимость на сайте', default=True)
    class Meta:
        verbose_name = 'Филиал'
        verbose_name_plural = 'Филиалы'




