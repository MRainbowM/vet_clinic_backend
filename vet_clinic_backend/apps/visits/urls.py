from django.urls import path

from . import views

app_name = 'visits'
urlpatterns = [
    path('', views.adm_list, name = 'adm_list'),
    path('appoitment/', views.client_app, name = 'client_app'),
    path('success/', views.client_message, name = 'client_message'),
    path('<int:id_visit>/', views.adm_detail, name = 'adm_detail'),
    # path('<int:id_direction>/<int:id>/', views.client_detail, name = 'client_detail'),
    # path('adm', views.index, name = 'index'),
    # path('adm/new_service', views.create, name = 'create'),
    # path('adm/<int:id_direction>/', views.category, name = 'category'),
    # path('adm/<int:id_direction>/<int:id>/', views.detail, name = 'detail'),
]