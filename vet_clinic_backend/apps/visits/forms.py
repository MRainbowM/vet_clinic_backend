from django import forms
from .models import *

class AppForm(forms.ModelForm):
    class Meta:
        model = Visit
        fields = ['name', 'phone', 'pet_name', 'services', 'pet_kind']

        widgets = {
            'name': forms.TextInput(attrs={'required':'True'}),
            'phone':forms.TextInput(attrs={'required':'True'}),
            'pet_name':forms.TextInput(),
            'services':forms.SelectMultiple(attrs={'required':'True','style':'min-height: 200px;', 'name':'services'}),
            'pet_kind':forms.Select()

        }