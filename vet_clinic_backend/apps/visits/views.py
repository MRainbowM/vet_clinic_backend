from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect
from .models import *
from filials.models import *
from clients.models import *
from workers.models import *
from .forms import AppForm

from django.contrib.auth.models import User
from django.core.paginator import Paginator

import datetime
from datetime import timedelta

def adm_list(request):
    if request.user.is_authenticated:
        if not request.user.groups.filter(name='managers').exists():
            raise PermissionDenied
    else:
        return HttpResponseRedirect('/accounts/login')
    visits = Visit.objects.all().order_by('-id')
    paginator = Paginator(visits, 4)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        visits_all = paginator.page(page)
    except(EmptyPage, InvalidPage):
        visits_all = paginator.page(paginator.num_pages)
    return render(request, 'visits/adm-visits.html', {'visits_all':visits_all})

def client_app(request):
    app = ''
    client = ''
    pets = ''
    if request.user.is_authenticated:
        # if request.user.groups.filter(name='doctors').exists():
        if request.user.groups.filter(name='managers').exists():
            app = 'adm'
        try: 
            client = Client.objects.get(user=request.user)
            pets = Pet.objects.filter(client = client)
            app = 'client'
        except:
            client = ''
    kinds_all = Kind.objects.all()
    filials_all = Filial.objects.filter(date_of_delete=None,visibility=True)
    cabinets_all = Cabinet.objects.filter(state_work=True)
    services_all = Service.objects.filter(date_of_delete=None, visibility=True).order_by('name')
    doctors_all = Worker.objects.filter(date_of_delete=None, visibility=True)
    visits_planer = []
    data = '['
    for service in services_all:
        data += '{"id":' + str(service.id) + ','
        data += '"cost": ' + str(service.cost) + ','
        data += '"duration":' + str(service.duration)
        if list(services_all).index(service) + 1 == len(services_all):
            data += '}'
        else:
            data += '},'
    data += ']'
    date_all = []
    i = 0
    date1 = datetime.date.today()
    while i < 8:
        date1 = date1 + timedelta(days=1)
        date_all.append(date1)
        i += 1
    if request.method == "POST":
        form = AppForm(request.POST)
        if form.is_valid():
            new_visit = form.save(commit=False)
            if client != '':
                new_visit.client = client
            if pets != '':
                id_pet = request.POST['pets']
                pet = Pet.objects.get(id=id_pet)
                new_visit.pet = pet
            id_filial = request.POST['filial']
            new_visit.filial = Filial.objects.get(id=id_filial)
            id_cabinet = request.POST['cabinet']
            new_visit.cabinet = Cabinet.objects.get(id=id_cabinet)
            id_doctor = request.POST['doctor']
            new_visit.doctor = Worker.objects.get(id=id_doctor)
            new_visit.cost = float(request.POST['cost'])
            new_visit.duration = request.POST['duration']
            time = request.POST['time']
            date = request.POST['date']
            DD = int(date[1:3])
            MM = int(date[4:6])
            YY = int(date[7:11])
            H = int(time[0:2])
            M = int(time[3:5])
            datetime1 = datetime.datetime(YY, MM, DD, H, M, 0)
            new_visit.date = datetime1
            new_visit.save()
            form.save_m2m()
            return redirect('visits:client_message')
        else:
            if app == 'adm':
                return render(request, 'visits/adm-appoitment.html', {'form':form,})
            elif app == 'client':
                return render(request, 'visits/appoitment.html', {'form':form,})
    else:
        form = AppForm()

    if app == 'adm':
        return render(request, 'visits/adm-appoitment.html', {'form':form, 'filials_all':filials_all,'cabinets_all':cabinets_all, 'services_all':services_all, 'kinds_all':kinds_all, 'doctors_all':doctors_all, 'date_all':date_all, 'data':data})
    elif app == 'client':
        return render(request, 'visits/appoitment.html', {'form':form,'client':client, 'filials_all':filials_all,'cabinets_all':cabinets_all, 'services_all':services_all, 'pets':pets, 'kinds_all':kinds_all, 'doctors_all':doctors_all, 'date_all':date_all, 'data':data})

def adm_detail(request, id_visit):
    if request.user.is_authenticated:
        if not request.user.groups.filter(name='managers').exists():
            raise PermissionDenied
    else:
        return HttpResponseRedirect('/accounts/login')

    visit = Visit.objects.get(id=id_visit)

def client_message(request):
    if request.user.is_authenticated:
        if request.user.groups.filter(name='managers').exists():
            return render(request, 'visits/adm-success_message.html', {})
        elif request.user.groups.filter(name='clients').exists():
            return render(request, 'visits/success_message.html', {})

    

