# Generated by Django 2.2.7 on 2019-12-22 04:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0003_auto_20191217_1636'),
        ('visits', '0007_auto_20191222_1157'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='visit',
            name='kind',
        ),
        migrations.RemoveField(
            model_name='visit',
            name='name_pet',
        ),
        migrations.AddField(
            model_name='visit',
            name='pet_kind',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='clients.Kind', verbose_name='Вид животного'),
        ),
        migrations.AddField(
            model_name='visit',
            name='pet_name',
            field=models.CharField(max_length=50, null=True, verbose_name='Кличка'),
        ),
        migrations.AlterField(
            model_name='visit',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='clients.Client', verbose_name='Клиент'),
        ),
        migrations.AlterField(
            model_name='visit',
            name='pet',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='clients.Pet', verbose_name='Питомец'),
        ),
    ]
