from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect

from .models import Sale
from workers.models import Worker

def client_list(request):
    sale_list = list(Sale.objects.filter(visibility=True).order_by('name'))
    if sale_list:
        sale_active = sale_list[0]
        sale_doctors = []
        doctors_all = list(Worker.objects.all())
        for doc in doctors_all:
            for service in sale_active.services.all():
                if service in doc.services.all():
                    sale_doctors.append(doc)
                    break
    else:
        sale_active = []

    return render(request, 'sales/sale.html', {'sale_list':sale_list, 'sale_active':sale_active, 'sale_doctors':sale_doctors})

def client_sale(request, id_sale):
    try: 
        sale_list = list(Sale.objects.filter(visibility=True).order_by('name'))
        for sale in sale_list:
            if sale.id == id_sale:
                sale_active = sale
        sale_doctors = []
        doctors_all = list(Worker.objects.all())
        for doc in doctors_all:
            for service in sale_active.services.all():
                if service in doc.services.all():
                    sale_doctors.append(doc)
                    break
    except:
        raise Http404("Акция не найдена :(")
    return render(request, 'sales/sale.html', {'sale_list':sale_list, 'sale_active':sale_active, 'sale_doctors':sale_doctors})




