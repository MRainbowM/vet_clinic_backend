from django.db import models
from django.core.validators import MinValueValidator
from services.models import Service

class Sale(models.Model):
    services = models.ManyToManyField(Service, verbose_name='Услуги')
    name = models.CharField('Название', max_length = 100)
    description = models.TextField('Описание', blank=True)
    start_date = models.DateField('Дата начала акции')
    end_date = models.DateField('Дата окончания акции')
    cost = models.FloatField('Стоимость', validators=[MinValueValidator(0)])
    duration = models.IntegerField('Продолжительность (мин.)',  default=0, validators=[MinValueValidator(0)])
    visibility = models.BooleanField('Видимость на сайте', default=True)
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)
    photo = models.ImageField('Фото', null=True, blank=True, upload_to="upload/")
    def __str__(self):
        return self.name