from django import forms
from django.core.exceptions import ValidationError
from reviews.models import *

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['content', 'phone', 'name', 'mail']

        widgets = {
            'content': forms.Textarea(attrs={'cols':30,'rows':10}),
            'phone': forms.TextInput(attrs={'type':'tel','id':'tel'}),
            'name': forms.TextInput(),
            'mail': forms.TextInput(attrs={'type':'email','id':'mail'}),
        }

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['comment', 'phone', 'name', 'mail']

        widgets = {
            'comment': forms.Textarea(attrs={'placeholder':'Комментарий','class':'con-form__comment', 'required':True, 'rows':5}),
            'phone': forms.TextInput(attrs={'placeholder':'Телефон','class':'con-form__tel', 'required':True}),
            'name': forms.TextInput(attrs={'placeholder':'Ваше имя','class':'con-form__tel', 'required':True}),
            'mail': forms.TextInput(attrs={'placeholder':'Почта','class':'con-form__tel','type':'email'})
        }


