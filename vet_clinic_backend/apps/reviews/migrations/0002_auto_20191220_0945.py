# Generated by Django 2.2.7 on 2019-12-20 01:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='mail',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Электронная почта'),
        ),
        migrations.AddField(
            model_name='review',
            name='phone',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Телефон'),
        ),
        migrations.AlterField(
            model_name='review',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='clients.Client', verbose_name='Клиент'),
        ),
    ]
