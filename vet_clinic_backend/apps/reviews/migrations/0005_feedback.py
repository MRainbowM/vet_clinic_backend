# Generated by Django 2.2.7 on 2019-12-22 18:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0004_review_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True, verbose_name='Имя')),
                ('phone', models.CharField(blank=True, max_length=20, null=True, verbose_name='Телефон')),
                ('mail', models.CharField(blank=True, max_length=50, null=True, verbose_name='Электронная почта')),
                ('comment', models.TextField(blank=True, verbose_name='Комментарий')),
            ],
        ),
    ]
