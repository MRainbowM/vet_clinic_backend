from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.db.models import Q

import datetime

from .forms import ReviewForm

from .models import *

def client_list(request):
    search_query = request.GET.get('search', '')
    if search_query:
        reviews_all = Review.objects.filter((Q(doctor__name__icontains=search_query) | Q(doctor__surname__icontains=search_query) | Q(doctor__patronymic__icontains=search_query) | Q(star__icontains=search_query) | Q(content__icontains=search_query) | Q(comment__icontains=search_query)), visibility=True, date_of_delete=None).order_by('-date')
    else:
        reviews_all = Review.objects.filter(visibility=True, date_of_delete=None).order_by('-date')

    directions_list = ['Услуги', 'Врачи']
    direction_active = 0

    paginator = Paginator(reviews_all, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    
    try:
        reviews_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        reviews_list = paginator.page(paginator.num_pages) 

    return render(request, 'reviews/reviews.html', {'reviews_list':reviews_list, 'directions_list':directions_list, 'direction_active':direction_active})

def client_category(request, category):
    if category == 1:
        reviews_all = Review.objects.exclude(service__id=None).filter(visibility=True, date_of_delete=None).order_by('-date')
        direction_active = 'Услуги'
    elif category == 2:
        reviews_all = Review.objects.exclude(doctor__id=None).filter(visibility=True, date_of_delete=None).order_by('-date')
        direction_active = 'Врачи'
    directions_list = ['Услуги', 'Врачи']

    paginator = Paginator(reviews_all, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    
    try:
        reviews_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        reviews_list = paginator.page(paginator.num_pages) 

    return render(request, 'reviews/reviews.html', {'reviews_list':reviews_list, 'directions_list':directions_list, 'direction_active':direction_active})

def client_detail(request, category, id):
    try: 
        review = Review.objects.get( id = id )
        directions_list = ['Услуги', 'Врачи']
        if category == 1:
            direction_active = 'Услуги'
        elif category == 2:
            direction_active = 'Врачи'
        review_photo = Review_photo.objects.filter(review=review)

    except:
        raise Http404("Отзыв не найден")

    return render(request, 'reviews/review-card.html', {'review':review, 'directions_list':directions_list, 'direction_active':direction_active, 'review_photo':review_photo})
 
def client_create(request):
    if request.user.is_authenticated:
        client = Client.objects.get(user=request.user)
    else:
        client = ''
    services_list = Service.objects.filter(visibility=True, date_of_delete=None).order_by('name')
    doctors_list = Worker.objects.filter(visibility=True, date_of_delete=None).order_by('surname')
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            new_review = form.save(commit=False)
            if client != '':
                new_review.client = client
            r = request.POST['review'] 
            if r == '1': #о враче
                id_doc = request.POST['doctors'] 
                new_review.doctor = Worker.objects.get(id=id_doc)
            elif r == '2':
                id_serv = request.POST['services'] 
                new_review.service = Service.objects.get(id=id_serv)
                s = Service.objects.get(id=id_serv)
            new_review.mail = request.POST['mail'] 
            if request.POST['tel']:
                new_review.phone = request.POST['tel']
            new_review.star = request.POST['star']
            new_review.date = datetime.datetime.now()

            new_review.save()
            return redirect('reviews:client_message')
        else:
            return render(request, 'reviews/add_review.html', {'form':form,'client':client,'services_list':services_list, 'doctors_list':doctors_list})
    else:
        form = ReviewForm()
    return render(request, 'reviews/add_review.html', {'form':form,'client':client,'services_list':services_list, 'doctors_list':doctors_list})

def client_message(request):
    return render(request, 'reviews/success_message.html', {})

 