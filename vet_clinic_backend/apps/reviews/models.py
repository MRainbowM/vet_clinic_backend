from django.db import models
from django.core.validators import MinValueValidator
from clients.models import Client
from workers.models import Worker
from services.models import Service


class Review(models.Model):
    client = models.ForeignKey(Client, on_delete = models.PROTECT, verbose_name='Клиент', null=True, blank=True)
    service = models.ForeignKey(Service, on_delete = models.PROTECT, verbose_name='Услуга', blank=True, null=True)
    doctor = models.ForeignKey(Worker, on_delete = models.PROTECT, verbose_name='Врач', blank=True, null=True)
    star = models.IntegerField('Оценка',  default=0, validators=[MinValueValidator(1)])
    content = models.TextField('Отзыв', blank=True)
    comment = models.TextField('Комментарий', blank=True)
    visibility = models.BooleanField('Видимость на сайте', default=True)
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)
    phone = models.CharField('Телефон', max_length = 20, null=True, blank=True)
    mail = models.CharField('Электронная почта', max_length = 50, null=True, blank=True)
    name = models.CharField('Имя', max_length = 50, null=True, blank=True)
    date = models.DateTimeField('Дата', null=True, blank=True)

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

class Review_photo(models.Model):
    photo = models.ImageField('Фото', null=True, blank=True, upload_to="upload/")
    review = models.ForeignKey(Review, on_delete = models.CASCADE, verbose_name='Отзыв')


class Feedback(models.Model):
    name = models.CharField('Имя', max_length = 50, null=True, blank=True)
    phone = models.CharField('Телефон', max_length = 20, null=True, blank=True)
    mail = models.CharField('Электронная почта', max_length = 50, null=True, blank=True)
    comment = models.TextField('Комментарий', blank=True)




