from django.urls import path

from . import views

app_name = 'reviews'

urlpatterns = [
    path('', views.client_list, name = 'client_list'),
    path('new/', views.client_create, name = 'client_create'),
    path('success/', views.client_message, name = 'client_message'),
    path('<int:category>/', views.client_category, name = 'client_category'),
    path('<int:category>/<int:id>/', views.client_detail, name = 'client_detail'),
]