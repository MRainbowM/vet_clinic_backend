from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.db.models import Q

from .models import *
from services.models import Service, Direction
from reviews.models import Review

from django.core.paginator import Paginator

def client_list(request):
    search_query = request.GET.get('search', '')
    if search_query:
        doctors_all = Worker.objects.filter((Q(name__icontains=search_query) | Q(surname__icontains=search_query) | Q(patronymic__icontains=search_query) | Q(position__name__icontains=search_query)), visibility=True, date_of_delete=None)
    else:
        doctors_all = Worker.objects.filter(visibility=True, date_of_delete=None).order_by('surname')
    
    directions_list = Direction.objects.all()
    direction_active = 0
    doctors_amount = doctors_all.count
    doctors_review = Review.objects.exclude(doctor__id=None)

    doctors_with_reviews = []
    for review in doctors_review:
        doctors_with_reviews.append(review.doctor)

    paginator = Paginator(doctors_all, 5)

    try:
        
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    
    try:
        doctors_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        doctors_list = paginator.page(paginator.num_pages) 

    return render(request, 'workers/doctors.html', {'doctors_list':doctors_list, 'directions_list':directions_list, 'direction_active':direction_active, 'doctors_review':doctors_review, 'doctors_with_reviews':doctors_with_reviews})
def client_category(request, id_direction):
    directions_list = Direction.objects.all()
    direction_active = Direction.objects.get(id=id_direction)
    doctors = []
    doctors_all = list(Worker.objects.filter(visibility=True, date_of_delete=None).order_by('surname'))
    doctors_review = Review.objects.exclude(doctor__id=None,visibility=True, date_of_delete=None)
    for doc in doctors_all:
        for service in doc.services.all():
            if service in direction_active.services.all():
                doctors.append(doc)
                break
    
    doctors_with_reviews = []
    for review in doctors_review:
        doctors_with_reviews.append(review.doctor)

    paginator = Paginator(doctors, 5)

    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    
    try:
        doctors_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        doctors_list = paginator.page(paginator.num_pages)

    return render(request, 'workers/doctors.html', {'doctors_list':doctors_list, 'directions_list':directions_list, 'direction_active':direction_active, 'doctors_review':doctors_review, 'doctors_with_reviews':doctors_with_reviews})

def client_detail(request, id_direction, id):
    try: 
        directions_list = Direction.objects.all()
        if id_direction == 0:
            direction_active = 0
        else:
            direction_active = Direction.objects.get(id=id_direction)
        doc = Worker.objects.get(id=id, visibility=True, date_of_delete=None)
        services_list = doc.services.filter(visibility=True, date_of_delete=None)
        reviews_list = Review.objects.filter(doctor__id=id,visibility=True, date_of_delete=None)
    except:
        raise Http404("Врач не найден")

    return render(request, 'workers/doctor-card.html', {'directions_list':directions_list, 'direction_active':direction_active, 'doc':doc, 'services_list':services_list,'reviews_list':reviews_list})