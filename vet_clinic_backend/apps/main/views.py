# from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect
from services.models import Direction
from sales.models import Sale
from workers.models import Worker
def index(request):
    direction_list = Direction.objects.filter(date_of_delete=None)[:10]
    sale_list = Sale.objects.filter(date_of_delete=None).filter(visibility=True)[:10]
    doctors_list = Worker.objects.filter(date_of_delete=None).filter(visibility=True)[:10]

    return render(request, 'main/index.html', {'direction_list':direction_list, 'sale_list':sale_list, 'doctors_list':doctors_list})

