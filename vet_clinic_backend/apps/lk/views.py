from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.db.models import Q
import datetime
from django.utils import timezone
from django.utils.timezone import utc

from .forms import PetForm, DocCommentForm

from django.core.exceptions import PermissionDenied

from django.contrib.auth.models import User

from clients.models import *
from visits.models import Visit
from workers.models import Worker
from services.models import Service
from filials.models import *


from django.core.serializers.json import DjangoJSONEncoder

import datetime
import time
from datetime import timedelta


def client_visits(request):
    lk = ''
    if request.user.is_authenticated:
        try: 
            worker = Worker.objects.get(user=request.user)
            lk = 'worker'
        except:
            try: 
                client = Client.objects.get(user=request.user)
                lk = 'client'
            except:
                return HttpResponseRedirect('/accounts/login')
    else:
        return HttpResponseRedirect('/accounts/login')
    
    if lk == 'client':
        visits_all = Visit.objects.filter(client__id=client.id).order_by('-date')
        visits_active = visits_all.filter(date__gte=datetime.date.today(), date_of_delete=None)
        visits_history_all = []
        for visit in visits_all:
            if visit not in visits_active:
                visits_history_all.append(visit)
        #Фильтры
        doctors_filter = []
        service_filter = []
        for visit in visits_history_all:
            if visit.doctor not in doctors_filter:
                doctors_filter.append(visit.doctor)

            if visit.services != None:
                for service in visit.services.all():
                    if service not in service_filter:
                        service_filter.append(service)

            if visit.sale != None:
                for service in visit.sale.services.all():
                    if service not in service_filter:
                        service_filter.append(service)
        pets_filter = Pet.objects.filter(client=client)
        #Обработка фильтров
        visits_history_view = []
        search_query = request.GET.get('doctor' )
        if search_query:
            d_f,s_f,p_f,date_from,date_before = 'Все','Все','Все','',''
            visits_history_filter = []
            if request.GET['doctor'] != 'Все':
                d_f = int(request.GET['doctor'])
            if request.GET['service'] != 'Все':
                s_f = int(request.GET['service'])         
            if request.GET['pet'] != 'Все':
                p_f = int(request.GET['pet'])
            if request.GET['date_from'] != '':
                date_from = request.GET['date_from']
            if request.GET['date_before'] != '':
                date_before = request.GET['date_before']

            if d_f != 'Все' and s_f == 'Все' and p_f == 'Все':
                for visit in visits_history_all:
                    if visit.doctor.id == int(d_f) and visit not in visits_history_filter:
                        visits_history_filter.append(visit)
            
            elif d_f != 'Все' and s_f == 'Все' and p_f != 'Все':   
                for visit in visits_history_all:
                    if visit.doctor.id == int(d_f) and visit.pet.id == int(p_f) and visit not in visits_history_filter:
                        visits_history_filter.append(visit) 
            
            elif d_f != 'Все' and s_f != 'Все' and p_f != 'Все': 
                service = Service.objects.get(id=int(s_f))
                for visit in visits_history_all:
                    if service in visit.services.all() and visit.doctor.id == int(d_f) and visit.pet.id == int(p_f) and visit not in visits_history_filter:
                        visits_history_filter.append(visit)
                    if visit.sale:
                        sale = visit.sale
                        for service_in_sale in sale.services.all():
                            if service == service_in_sale and visit.doctor.id == int(d_f) and visit.pet.id == int(p_f) and visit not in visits_history_filter:
                                visits_history_filter.append(visit)
            
            elif d_f != 'Все' and s_f != 'Все' and p_f == 'Все':
                service = Service.objects.get(id=int(s_f))
                for visit in visits_history_all:
                    if service in visit.services.all() and visit.doctor.id == int(d_f) and visit not in visits_history_filter:
                        visits_history_filter.append(visit)
                    if visit.sale:
                        sale = visit.sale
                        for service_in_sale in sale.services.all():
                            if service == service_in_sale and visit.doctor.id == int(d_f) and visit not in visits_history_filter:
                                visits_history_filter.append(visit)

            elif d_f == 'Все' and s_f != 'Все' and p_f != 'Все':
                service = Service.objects.get(id=int(s_f))
                for visit in visits_history_all:
                    if service in visit.services.all() and visit.pet.id == int(p_f) and visit not in visits_history_filter:
                        visits_history_filter.append(visit)
                    if visit.sale:
                        sale = visit.sale
                        for service_in_sale in sale.services.all():
                            if service == service_in_sale and visit.pet.id == int(p_f) and visit not in visits_history_filter:
                                visits_history_filter.append(visit)

            elif d_f == 'Все' and s_f != 'Все' and p_f == 'Все':
                service = Service.objects.get(id=int(s_f))
                for visit in visits_history_all:
                    if service in visit.services.all() and visit not in visits_history_filter:
                        visits_history_filter.append(visit)
                    if visit.sale:
                        sale = visit.sale
                        for service_in_sale in sale.services.all():
                            if service == service_in_sale and visit not in visits_history_filter:
                                visits_history_filter.append(visit)

            elif d_f == 'Все' and s_f == 'Все' and p_f != 'Все':
                for visit in visits_history_all:
                    if visit.pet.id == int(p_f) and visit not in visits_history_filter:
                        visits_history_filter.append(visit)

            elif d_f == 'Все' and s_f == 'Все' and p_f == 'Все':
                visits_history_filter = visits_history_all

            for visit in visits_history_filter:
                date1 = visit.date
                if date_from != '' and date_before == '':
                    date2 = datetime.datetime.strptime(date_from, '%Y-%m-%d').replace(tzinfo=utc)
                    if date1 >= date2:
                        visits_history_view.append(visit)

                if date_from != '' and date_before != '':
                    date2 = datetime.datetime.strptime(date_from, '%Y-%m-%d').replace(tzinfo=utc)
                    date3 = datetime.datetime.strptime(date_before, '%Y-%m-%d').replace(tzinfo=utc)
                    if date1 >= date2 and date1 <= date3:
                        visits_history_view.append(visit)

                if date_from == '' and date_before != '':
                    date3 = datetime.datetime.strptime(date_before, '%Y-%m-%d').replace(tzinfo=utc) 
                    if date1 <= date3:
                        visits_history_view.append(visit)

                if date_from == '' and date_before == '':
                    visits_history_view.append(visit)
        else:
            d_f,s_f,p_f,date_from,date_before = 'Все','Все','Все','',''
            visits_history_view = visits_history_all
        #Пагинация
        paginator = Paginator(visits_history_view, 4)
        try:
            page = int(request.GET.get('page', '1'))
        except:
            page = 1
        try:
            visits_history = paginator.page(page)
        except(EmptyPage, InvalidPage):
            visits_history = paginator.page(paginator.num_pages)
        return render(request, 'lk/lk-visits.html', {'visits_all':visits_all, 'client':client, 'visits_active':visits_active, 'visits_history':visits_history, 'doctors_filter':doctors_filter, 'service_filter':service_filter, 'pets_filter':pets_filter, 'd_f':d_f, 's_f':s_f, 'p_f':p_f, 'date_from':date_from ,'date_before':date_before})
    elif lk == 'worker':
        visits_all = Visit.objects.filter(doctor=worker, date_of_delete=None)
        services_all = []
        clients_all = []
        for visit in visits_all:
            if visit.services != None:
                for service in visit.services.all():
                    if service not in services_all:
                        services_all.append(service)

            if visit.sale != None:
                for service in visit.sale.services.all():
                    if service not in services_all:
                        services_all.append(service)
            
            if visit.client != None and visit.client not in clients_all:
                clients_all.append(visit.client)

        visits_planer = []
        
        data = '['
        visits = list(visits_all)
        for visit in visits_all:
            serv = ''
            for service in visit.services.all():
                serv += str(service.name) + ', '

            data += '{"id":' + str(visit.id) + ','
            data += '"name":"' + serv + '",'
            data += '"from":' + str(time.mktime(visit.date.timetuple()) * 1000) + ','
            data += '"duration":' + str(visit.duration)
            if visits.index(visit) + 1 ==  len(visits_all):

                data += '}'
            else:
                data += '},'
        data += ']'
        search_query = request.GET.get('date' )
       
        if search_query:
            d = request.GET['date']
            if d != '':
                current_date = datetime.datetime.fromtimestamp(float(d)) 
        else:
            current_date = datetime.datetime.today()

        date1 = datetime.datetime(current_date.year, current_date.month, current_date.day, 8, 0)
        date2 = datetime.datetime(current_date.year, current_date.month, current_date.day, 20, 0)
        planer_from = str(time.mktime(date1.timetuple()) * 1000)
        planer_to = str(time.mktime(date2.timetuple()) * 1000)
        week = timedelta(days=7)
        prev_week = current_date - week
        prev_week = str(time.mktime(prev_week.timetuple()))
        next_week = current_date + week
        next_week = str(time.mktime(next_week.timetuple()))
        return render(request, 'lk/d-lk-visits.html', {'visits_all':visits_all, 'services_all':services_all, 'clients_all':clients_all, 'data':data, 'planer_from':planer_from, 'planer_to':planer_to, 'prev_week':prev_week, 'next_week':next_week})

def client_visit_card(request, id_visit):
    lk = ''
    if request.user.is_authenticated:
        try: 
            worker = Worker.objects.get(user=request.user)
            lk = 'worker'
        except:
            try: 
                client = Client.objects.get(user=request.user)
                lk = 'client'
            except:
                return HttpResponseRedirect('/accounts/login')
    else:
        return HttpResponseRedirect('/accounts/login')
    if lk == 'client':
        try:
            visit = Visit.objects.get(id = id_visit)
            if visit.client != client:
                raise PermissionDenied
        except:
            raise Http404("Прием не найден")

        return render(request, 'lk/lk-visit-card.html', {'client':client,'visit':visit})
    elif lk == 'worker':
        try:
            visit = Visit.objects.get(id = id_visit)
            if visit.doctor != worker:
                raise PermissionDenied
        except:
            raise Http404("Прием не найден")

        if request.method == "POST":
            form = DocCommentForm(request.POST,instance=visit)
            if form.is_valid():
                new_visit = form.save()
                return redirect('lk:client_visits')
            else:
                return render(request, 'lk/d-lk-visit-card.html', {'form':form, 'visit':visit})
        else:
            form = DocCommentForm(instance=visit)



        return render(request, 'lk/d-lk-visit-card.html', {'form':form, 'visit':visit})

def client_visit_card_edit(request, id_visit):
    if request.user.is_authenticated:
        try: 
            worker = Worker.objects.get(user=request.user)
        except:
            return HttpResponseRedirect('/accounts/login')
    else:
        return HttpResponseRedirect('/accounts/login')
    try:
        visit = Visit.objects.get(id = id_visit)
        if visit.doctor != worker:
            raise PermissionDenied
    except:
        raise Http404("Прием не найден")
    filials_all = Filial.objects.filter(date_of_delete=None, visibility=True)
    cabinets_all = Cabinet.objects.filter(state_work=True)
    services_all = Service.objects.filter(date_of_delete=None, visibility=True)
 
    return render(request, 'lk/d-lk-visit-card__edit.html', {'visit':visit, 'filials_all':filials_all,'cabinets_all':cabinets_all, 'services_all':services_all})

def client_pets(request):
    try:
        client = Client.objects.get(user=request.user)
        pets_list = Pet.objects.filter(client = client)
        
    except:
        return HttpResponseRedirect('/accounts/login')


    return render(request, 'lk/lk-pets.html', {'client':client, 'pets_list':pets_list})

def client_profile(request):
    try:
        client = Client.objects.get(user=request.user)
        
    except:
        return HttpResponseRedirect('/accounts/login')

    return render(request, 'lk/lk-profile.html', {'client':client})

def client_history(request, id_pet):
    try:
        client = Client.objects.get(user=request.user)
    except:
        return HttpResponseRedirect('/accounts/login')
    pet = Pet.objects.get(id = id_pet)
    visits_all = Visit.objects.filter(pet = pet).order_by('-date')
    for visit in visits_all:
        if visit.client != client:
            raise PermissionDenied
    #Фильтры
    doctors_filter = []
    service_filter = []
    for visit in visits_all:
        if visit.doctor not in doctors_filter:
            doctors_filter.append(visit.doctor)

        if visit.services != None:
            for service in visit.services.all():
                if service not in service_filter:
                    service_filter.append(service)

        if visit.sale != None:
            for service in visit.sale.services.all():
                if service not in service_filter:
                    service_filter.append(service)
    #Обработка фильтров
    visits_history_view = []
    search_query = request.GET.get('doctor' )

    if search_query:
        d_f,s_f,date_from,date_before = 'Все','Все','',''
        visits_history_filter = []
        if request.GET['doctor'] != 'Все':
            d_f = int(request.GET['doctor'])
        if request.GET['service'] != 'Все':
            s_f = int(request.GET['service'])         
        if request.GET['date_from'] != '':
            date_from = request.GET['date_from']
        if request.GET['date_before'] != '':
            date_before = request.GET['date_before']

        if d_f != 'Все' and s_f == 'Все':
            for visit in visits_all:
                if visit.doctor.id == int(d_f) and visit not in visits_history_filter:
                    visits_history_filter.append(visit)
        
        elif d_f != 'Все' and s_f != 'Все': 
            service = Service.objects.get(id=int(s_f))
            for visit in visits_all:
                if service in visit.services.all() and visit.doctor.id == int(d_f) and visit not in visits_history_filter:
                    visits_history_filter.append(visit)
                if visit.sale:
                    sale = visit.sale
                    for service_in_sale in sale.services.all():
                        if service == service_in_sale and visit.doctor.id == int(d_f) and visit not in visits_history_filter:
                            visits_history_filter.append(visit)

        elif d_f == 'Все' and s_f != 'Все':
            service = Service.objects.get(id=int(s_f))
            for visit in visits_all:
                if service in visit.services.all() and visit not in visits_history_filter:
                    visits_history_filter.append(visit)
                if visit.sale:
                    sale = visit.sale
                    for service_in_sale in sale.services.all():
                        if service == service_in_sale and visit not in visits_history_filter:
                            visits_history_filter.append(visit)

        elif d_f == 'Все' and s_f == 'Все':
            for visit in visits_all:
                if visit not in visits_history_filter:
                    visits_history_filter.append(visit)

        for visit in visits_history_filter:
            date1 = visit.date
            if date_from != '' and date_before == '':
                date2 = datetime.datetime.strptime(date_from, '%Y-%m-%d').replace(tzinfo=utc)
                if date1 >= date2:
                    visits_history_view.append(visit)
            if date_from != '' and date_before != '':
                date2 = datetime.datetime.strptime(date_from, '%Y-%m-%d').replace(tzinfo=utc)
                date3 = datetime.datetime.strptime(date_before, '%Y-%m-%d').replace(tzinfo=utc)
                if date1 >= date2 and date1 <= date3:
                    visits_history_view.append(visit)
            if date_from == '' and date_before != '':
                date3 = datetime.datetime.strptime(date_before, '%Y-%m-%d').replace(tzinfo=utc) 
                if date1 <= date3:
                    visits_history_view.append(visit)

            if date_from == '' and date_before == '':
                visits_history_view.append(visit)

    else:
        d_f,s_f,date_from,date_before = 'Все','Все','',''
        visits_history_view = visits_all

    paginator = Paginator(visits_history_view, 4)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    
    try:
        visits = paginator.page(page)
    except(EmptyPage, InvalidPage):
        visits = paginator.page(paginator.num_pages)

    return render(request, 'lk/lk-history.html', {'client':client,  'visits':visits, 'pet':pet, 'doctors_filter':doctors_filter, 'service_filter':service_filter, 'd_f':d_f, 's_f':s_f, 'date_from':date_from ,'date_before':date_before})

def client_pet_create(request):
    try:
        client = Client.objects.get(user=request.user)
    except:
        return HttpResponseRedirect('/accounts/login')
    operation = 'create'

    if request.method == "POST":
        form = PetForm(request.POST)
        if form.is_valid():
            new_pet = form.save(commit=False)
            new_pet.sex = request.POST['sex']
            new_pet.date_of_birth = request.POST['date_of_birth']
            new_pet.client = client
            new_pet.save()
            return redirect('lk:client_pets')
        else:
            return render(request, 'lk/lk-pet__edit.html', {'form':form, 'operation':operation})
    else:
        form = PetForm()

    return render(request, 'lk/lk-pet__edit.html', {'form':form})

def client_pet_edit(request, id_pet):
    try:
        client = Client.objects.get(user=request.user)
    except:
        return HttpResponseRedirect('/accounts/login')
    pet = Pet.objects.get(id=id_pet)
    if pet.client != client:
        raise PermissionDenied
    operation = 'edit'
    if request.method == "POST":
        form = PetForm(request.POST, instance=pet)
        if form.is_valid():
            new_pet = form.save(commit=False)
            new_pet.sex = request.POST['sex']
            new_pet.date_of_birth = request.POST['date_of_birth']
            new_pet.client = client
            new_pet.save()
            return redirect('lk:client_pets')
        else:
            return render(request, 'lk/lk-pet__edit.html', {'form':form})
    else:
        form = PetForm(instance=pet)

    return render(request, 'lk/lk-pet__edit.html', {'form':form, 'pet':pet, 'operation':operation})

