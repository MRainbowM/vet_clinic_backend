from django import forms
# from django.core.exceptions import ValidationError
from clients.models import Pet
from visits.models import Visit

class PetForm(forms.ModelForm):
    class Meta:
        model = Pet
        fields = ['name', 'kind', 'date_of_birth']

        widgets = {
            'name': forms.TextInput(),
            'kind':forms.Select(),
            'date_of_birth':forms.DateInput()

        }

class ClientForm(forms.ModelForm):
    class Meta:
        model = Pet
        fields = ['name', 'kind', 'date_of_birth']

        widgets = {
            'name': forms.TextInput(),
            'kind':forms.Select(),
            'date_of_birth':forms.DateInput()

        }

class DocCommentForm(forms.ModelForm):
    class Meta:
        model = Visit
        fields = ['comment']

        widgets = {
            'comment': forms.Textarea(attrs={'rows':10, 'style':"width:100%"})
        }


