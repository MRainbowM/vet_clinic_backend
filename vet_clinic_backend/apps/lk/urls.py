from django.urls import path

from . import views

app_name = 'lk'

urlpatterns = [
    path('', views.client_visits, name = 'client_visits'),
    path('visit/<int:id_visit>/', views.client_visit_card, name = 'client_visit_card'),
    path('visit/<int:id_visit>/edit', views.client_visit_card_edit, name = 'client_visit_card_edit'),
    path('pets/new', views.client_pet_create, name = 'client_pet_create'),
    path('pets/', views.client_pets, name = 'client_pets'),
    path('pets/<int:id_pet>/edit/', views.client_pet_edit, name = 'client_pet_edit'),
    path('profile/', views.client_profile, name = 'client_profile'),
    path('history/<int:id_pet>/', views.client_history, name = 'client_history'),
]