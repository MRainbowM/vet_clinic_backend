from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm
from django.contrib.auth.models import Group
from clients.models import Client

from django.contrib.auth import authenticate



def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.username = form.cleaned_data.get('username')
            # new_user.username = form.cleaned_data['username']
            # new_user.set_password(form.cleaned_data['password'])
            new_user.save()




            new_user.groups.add(Group.objects.get(name='clients'))
            new_client = Client()
            new_client.user = new_user
            new_client.save()
            # if request.user.is_authenticated:
            #     client = Client.objects.get(user=request.user)
            # else:
            #     return HttpResponseRedirect('/accounts/login')

            # passw = form.cleaned_data['password2']

            # login_user = authenticate(request, username=form.cleaned_data['username'], password=form.cleaned_data['password2'])
            # if login_user:
            #     # login(request, login_user)
            #     return HttpResponseRedirect('/lk/')

            # else:
            #     return redirect('main:index')




            # form.save()
            # username = form.cleaned_data.get('username')

            # messages.success(request, f'Аккаунт для {username} успешно создан')


            # return redirect('main:index')


    else:
        form = UserRegisterForm()
    return render(request, 'registration/register.html', {'form':form})
