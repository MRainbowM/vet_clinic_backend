from django.db import models
from django.core.validators import MinValueValidator

class Service(models.Model):
    name = models.CharField('Название', max_length = 100)
    description = models.TextField('Описание', blank=True)
    duration = models.IntegerField('Продолжительность',  default=0, validators=[MinValueValidator(0)])
    cost = models.FloatField('Стоимость', validators=[MinValueValidator(0)])
    # workers = models.ManyToManyField(Worker)
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)
    visibility = models.BooleanField('Видимость на сайте', default=True)
    nurse = models.BooleanField('Необходимость медсестры', default=False)
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

class Direction(models.Model):
    name = models.CharField('Название', max_length = 50)
    services = models.ManyToManyField(Service, blank=True)
    photo = models.ImageField('Фото', null=True, blank=True, upload_to="upload/")
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Направление'
        verbose_name_plural = 'Направления'

