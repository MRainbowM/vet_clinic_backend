from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.db.models import Q

from .forms import *
from .models import Service, Direction
from workers.models import Worker
from reviews.models import Review


def client_list(request):
    search_query = request.GET.get('search', '')
    if search_query:
        services_all = Service.objects.filter((Q(name__icontains=search_query) | Q(cost__icontains=search_query) | Q(id__icontains=search_query)), visibility=True, date_of_delete=None)
    else:
        services_all = Service.objects.filter(visibility=True, date_of_delete=None).order_by('id')

    directions_list = Direction.objects.all()
    direction_active = 0
    services_amount = services_all.count

    paginator = Paginator(services_all, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    
    try:
        services_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        services_list = paginator.page(paginator.num_pages) 

    return render(request, 'services/services.html', {'services_list':services_list, 'directions_list':directions_list, 'direction_active':direction_active, 'services_amount':services_amount})

def client_category(request, id_direction):
    directions_list = Direction.objects.all()
    direction_active = Direction.objects.get(id=id_direction)
    services_in_dir = direction_active.services.filter(visibility=True, date_of_delete=None).order_by('id')
    services_amount = Service.objects.filter(visibility=True, date_of_delete=None).count

    paginator = Paginator(services_in_dir, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    
    try:
        services_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        services_list = paginator.page(paginator.num_pages) 

    return render(request, 'services/services.html', {'services_list':services_list, 'directions_list':directions_list, 'direction_active':direction_active, 'services_amount':services_amount})

def client_detail(request, id_direction, id):
    try: 
        service = Service.objects.get( id = id )
        directions_list = Direction.objects.all()
        if id_direction==0:
            direction_active = 0
        else:
            direction_active = Direction.objects.get(id=id_direction)
        service_directions = Direction.objects.filter(services__id=service.id)
        service_doctors = Worker.objects.filter(services__id=service.id)
        services_amount = Service.objects.filter(visibility=True, date_of_delete=None).count
        # services_reviews = Review.objects.get( service__id=service.id)
        services_reviews = Review.objects.filter( service__id=service.id )

    except:
        raise Http404("услуга не найдена")

    return render(request, 'services/service-card.html', {'service':service, 'directions_list':directions_list, 'service_directions':service_directions, 'service_doctors':service_doctors, 'direction_active':direction_active, 'services_amount':services_amount, 'services_reviews':services_reviews})

def index(request):
    directions_list = Direction.objects.all()
    services_all = Service.objects.filter(visibility=True, date_of_delete=None).order_by('id')
    direction_active = 0
    services_amount = services_all.count

    paginator = Paginator(services_all, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    
    try:
        services_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        services_list = paginator.page(paginator.num_pages)

    return render(request, 'services/adm-services.html', {'services_list':services_list, 'directions_list':directions_list, 'direction_active':direction_active, 'services_amount':services_amount})

def category(request, id_direction):
    directions_list = Direction.objects.all()
    direction_active = Direction.objects.get( id=id_direction )
    services_in_dir = direction_active.services.filter(visibility=True, date_of_delete=None).order_by('id')
    services_amount = Service.objects.filter(visibility=True, date_of_delete=None).count

    paginator = Paginator(services_in_dir, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    
    try:
        services_list = paginator.page(page)
    except(EmptyPage, InvalidPage):
        services_list = paginator.page(paginator.num_pages) 

    return render(request, 'services/adm-services.html', {'services_list':services_list, 'directions_list':directions_list, 'direction_active':direction_active, 'services_amount':services_amount})

def detail(request, id_direction, id):
    try: 
        service = Service.objects.get( id = id )
        directions_list = Direction.objects.all()
        if id_direction==0:
            direction_active = 0
        else:
            direction_active = Direction.objects.get(id=id_direction)
        service_directions = Direction.objects.filter(services__id=service.id)
        service_doctors = Worker.objects.filter(services__id=service.id)
        services_amount = Service.objects.filter(visibility=True, date_of_delete=None).count

    except:
        raise Http404("услуга не найдена")

    return render(request, 'services/adm-service-card.html', {'service':service, 'directions_list':directions_list, 'service_directions':service_directions, 'service_doctors':service_doctors, 'direction_active':direction_active, 'services_amount':services_amount})

def create(request):
    directions_list = Direction.objects.all()
    services_amount = Service.objects.filter(visibility=True, date_of_delete=None).count
        
    if request.method == "POST":
        form = ServiceForm(request.POST)
        if form.is_valid():
            service = form.save(commit=False)
            # service.name = service_name
            service.save()
            return redirect('detail', id=service.id)
    else:
        form = ServiceForm()

    return render(request, 'services/adm-service-card__edit.html', {'directions_list':directions_list,'services_amount':services_amount})
    
