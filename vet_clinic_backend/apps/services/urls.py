from django.urls import path

from . import views

app_name = 'services'

urlpatterns = [
    path('', views.client_list, name = 'client_list'),
    path('<int:id_direction>/', views.client_category, name = 'client_category'),
    path('<int:id_direction>/<int:id>/', views.client_detail, name = 'client_detail'),
    path('adm', views.index, name = 'index'),
    path('adm/new_service', views.create, name = 'create'),
    path('adm/<int:id_direction>/', views.category, name = 'category'),
    path('adm/<int:id_direction>/<int:id>/', views.detail, name = 'detail'),
]