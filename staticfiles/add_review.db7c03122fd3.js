;(function(){
    var rb1 = document.getElementById('rb1')
    var rb2 = document.getElementById('rb2')
    var selRb1 = document.querySelector('[data-show-by="rb1"]')
    var selRb2 = document.querySelector('[data-show-by="rb2"]')
    function onChange() {
        if (rb1.checked) {
            selRb1.style.display = null
            selRb2.style.display = 'none'
        } else if (rb2.checked) {
            selRb1.style.display = 'none'
            selRb2.style.display = null
        } else {
            selRb1.style.display = null
            selRb2.style.display = null
        }
    }
    onChange()
    rb1.addEventListener('input', onChange)
    rb2.addEventListener('input', onChange)
})()