-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_vet_clinic
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `visits_visit`
--

DROP TABLE IF EXISTS `visits_visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visits_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  `comment` longtext NOT NULL,
  `cost` double NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `cabinet_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `filial_id` int(11) NOT NULL,
  `pet_id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `pet_kind_id` int(11) DEFAULT NULL,
  `pet_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `visits_visit_cabinet_id_925d2914_fk_filials_cabinet_id` (`cabinet_id`),
  KEY `visits_visit_doctor_id_ecb90376_fk_workers_worker_id` (`doctor_id`),
  KEY `visits_visit_filial_id_8ab645ff_fk_filials_filial_id` (`filial_id`),
  KEY `visits_visit_sale_id_e5c1371f_fk_sales_sale_id` (`sale_id`),
  KEY `visits_visit_client_id_28388262_fk_clients_client_id` (`client_id`),
  KEY `visits_visit_pet_id_fcf877b2_fk_clients_pet_id` (`pet_id`),
  KEY `visits_visit_pet_kind_id_00a1ab7b` (`pet_kind_id`),
  CONSTRAINT `visits_visit_cabinet_id_925d2914_fk_filials_cabinet_id` FOREIGN KEY (`cabinet_id`) REFERENCES `filials_cabinet` (`id`),
  CONSTRAINT `visits_visit_client_id_28388262_fk_clients_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients_client` (`id`),
  CONSTRAINT `visits_visit_doctor_id_ecb90376_fk_workers_worker_id` FOREIGN KEY (`doctor_id`) REFERENCES `workers_worker` (`id`),
  CONSTRAINT `visits_visit_filial_id_8ab645ff_fk_filials_filial_id` FOREIGN KEY (`filial_id`) REFERENCES `filials_filial` (`id`),
  CONSTRAINT `visits_visit_pet_id_fcf877b2_fk_clients_pet_id` FOREIGN KEY (`pet_id`) REFERENCES `clients_pet` (`id`),
  CONSTRAINT `visits_visit_pet_kind_id_00a1ab7b_fk_clients_kind_id` FOREIGN KEY (`pet_kind_id`) REFERENCES `clients_kind` (`id`),
  CONSTRAINT `visits_visit_sale_id_e5c1371f_fk_sales_sale_id` FOREIGN KEY (`sale_id`) REFERENCES `sales_sale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-24  1:28:06
